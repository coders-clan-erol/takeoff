# frozen_string_literal: true

require 'spec_helper'
require './lib/command_runner'

describe CommandRunner do
  before do
    enable_dry_run

    allow_open3_to_return(true)
  end

  describe '#run_command_on_roles' do
    it 'runs commands with knife' do
      expect(Open3).to receive(:popen3).with("echo bundle exec knife ssh -e -a fqdn 'roles:gstg-base-fe-api' 'date'")

      described_class.run_command_on_roles('gstg-base-fe-api', 'date')
    end
  end
end
