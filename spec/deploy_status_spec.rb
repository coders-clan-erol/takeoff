# frozen_string_literal: true

require 'spec_helper'
require './lib/deploy_status'
require './config/roles'

describe DeployStatus do
  let(:deploy_status) { described_class.new }

  it 'shows 100% if it finished or there is no progress' do
    allow(File).to receive(:exist?).and_return(false)

    expect(deploy_status.as_percentage).to eq('100% deployment finished.')
  end

  context 'when .resume file exists' do
    before do
      allow(File).to receive(:exist?).and_return(true)
      allow(deploy_status).to receive(:steps).and_return(YAML.load_file('./spec/fixtures/steps.yml'))
    end

    it 'shows the correct percentage for the migrations step' do
      allow(deploy_status).to receive(:load_resumer_state).and_return(
        current_step: 7,
        substeps: [],
        start_time: '2000-06-21 12:30:55 UTC'
      )

      expect(deploy_status.as_percentage).to eq('34% deployed (running migrations)')
    end

    it 'shows the correct percentage for the mediator step' do
      allow(deploy_status).to receive(:load_resumer_state).and_return(
        current_step: 9,
        substeps: %w[role1 role2 role3],
        start_time: '2000-06-21 12:30:55 UTC'
      )

      expect(deploy_status.as_percentage).to eq('56% deployed (running deploy_mediator)')
    end

    it 'shows the correct percentage while on the last step' do
      role_count = Roles.new('gprd').regular_no_gitaly.count

      allow(deploy_status).to receive(:load_resumer_state).and_return(
        current_step: 16,
        substeps: ['role'] * role_count,
        start_time: '2000-06-21 12:30:55 UTC'
      )

      expect(deploy_status.as_percentage).to eq('100% deployed (running version_check)')
    end
  end
end
