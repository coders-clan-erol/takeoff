# frozen_string_literal: true

require 'spec_helper'
require './lib/progress_runner'

describe ProgressRunner do
  let(:subject) { ProgressRunner.new('echo "done"', title: 'Doing') }

  it 'runs a given command' do
    expect(Open3).to receive(:popen3).with('echo "done"')

    subject.execute
  end

  it "doesn't execute commands in dry_run mode" do
    expect(Open3).to receive(:popen3).with('echo echo "done"')

    subject.execute(dry_run: true)
  end

  it 'displays the title' do
    expect { subject.execute }.to output(/Doing/).to_stderr
  end

  it 'prints commands in verbose mode' do
    expect { subject.execute(verbose: true) }.to output(/> echo "done"/).to_stdout
  end

  it 'renders a spinner' do
    expect { subject.execute }.to output(/\e\[1G⠋.*✓/).to_stderr
  end

  it 'appends time elapsed to spinner' do
    expect { subject.execute }.to output(/0s ago/).to_stderr
  end

  it 'displays mins elapsed if needed' do
    allow(subject).to receive(:duration).and_return(60 * 15.1)

    expect { subject.execute }.to output(/15 minutes ago/).to_stderr
  end

  it 'displays hours elapsed if needed' do
    allow(subject).to receive(:duration).and_return(60 * 90)

    expect { subject.execute }.to output(/1.5 hours ago/).to_stderr
  end

  it 'displays days elapsed if needed' do
    allow(subject).to receive(:duration).and_return(60 * 60 * 36)

    expect { subject.execute }.to output(/1.5 days ago/).to_stderr
  end

  it 'appends duration header on completion' do
    allow(subject).to receive(:duration).and_return(60 * 24)

    expect { subject.execute }.to output(/Doing[^\(]*\(24 minutes\)$/).to_stderr
  end

  context 'on error' do
    let(:command) { 'echo "log" && >&2 echo "oops" && exit 1' }
    let(:subject) { ProgressRunner.new(command, title: 'Fail') }

    it 'raises error' do
      expect { subject.execute }.to raise_error(RuntimeError)
    end

    it 'prints output to stdout' do
      expect do
        begin
          subject.execute
        rescue RuntimeError
        end
      end.to output(/log/).to_stdout
    end

    it 'prints errors to stderr' do
      expect do
        begin
          subject.execute
        rescue RuntimeError
        end
      end.to output(/oops/).to_stderr
    end
  end

  context 'without a title' do
    let(:subject) { ProgressRunner.new('echo now') }

    it 'uses the command as the title' do
      expect { subject.execute }.to output(/echo now/).to_stderr
    end
  end
end
