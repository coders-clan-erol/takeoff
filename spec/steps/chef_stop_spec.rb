# frozen_string_literal: true

require 'spec_helper'
require './lib/steps/chef_stop'

describe Steps::ChefStop do
  before { enable_dry_run }

  subject { described_class.new(Roles.new('gstg')) }

  it 'outputs the right command' do
    command = <<~COMMAND.strip.tr("\n", ' ')
      bundle exec knife ssh -e -a fqdn roles:gstg-base-stor-nfs OR
      roles:gstg-base-be-sidekiq OR roles:gstg-base-be-mailroom OR
      roles:gstg-base-fe-web OR roles:gstg-base-fe-api OR
      roles:gstg-base-fe-git OR roles:gstg-base-fe-registry OR
      roles:gstg-base-deploy-node sudo service chef-client stop
    COMMAND

    expect { subject.run }.to output(/#{command}/).to_stdout_from_any_process
  end
end
