# frozen_string_literal: true

require 'spec_helper'
require './lib/steps/deploy_mediator'

describe Steps::DeployMediator do
  before do
    enable_dry_run

    # Avoid Threads here so the output is not random
    allow_any_instance_of(described_class).to receive(:run_steps!) do |mediator|
      mediator.send(:concurrent_steps).map { |step| step.run! }
    end
  end

  subject { described_class.new(Roles.new('gstg'), version: '10.0.2-ee.0', resumer: Resumer.new(false)) }

  context 'no custom node' do
    it 'outputs the right command' do
      expect { subject.run }.to output(command).to_stdout_from_any_process
    end

    def command
      command = <<~COMMAND
        bundle exec knife search 'roles:gstg-base-fe-registry' -a fqdn -Fjson
        bundle exec knife search 'roles:gstg-base-deploy-node' -a fqdn -Fjson
        bundle exec knife ssh -e -a fqdn 'fqdn:fqdn1' 'sudo cat /opt/gitlab/embedded/service/gitlab-rails/GITLAB_PAGES_VERSION 2>/dev/null'
        bundle exec knife ssh -e -a fqdn 'fqdn:fqdn2' 'sudo cat /opt/gitlab/embedded/service/gitlab-rails/GITLAB_PAGES_VERSION 2>/dev/null'
        bundle exec knife search 'roles:gstg-base-fe-registry' -a fqdn -Fjson
        bundle exec knife search 'roles:gstg-base-deploy-node' -a fqdn -Fjson
        bundle exec knife ssh -e -a fqdn 'fqdn:fqdn1' 'sudo cat /opt/gitlab/embedded/service/gitlab-rails/GITLAB_WORKHORSE_VERSION 2>/dev/null'
        bundle exec knife ssh -e -a fqdn 'fqdn:fqdn2' 'sudo cat /opt/gitlab/embedded/service/gitlab-rails/GITLAB_WORKHORSE_VERSION 2>/dev/null'
        bundle exec knife ssh -e -a fqdn roles:gstg-base-be-sidekiq sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure && sudo gitlab-ctl hup unicorn
        bundle exec knife ssh -e -a fqdn roles:gstg-base-be-sidekiq sudo gitlab-ctl restart sidekiq-cluster
        bundle exec knife ssh -e -a fqdn roles:gstg-base-be-sidekiq sudo gitlab-ctl restart nginx
        bundle exec knife ssh -e -a fqdn roles:gstg-base-be-mailroom sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure
        bundle exec knife ssh -e -a fqdn roles:gstg-base-be-mailroom sudo gitlab-ctl restart
        bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-web sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure && sudo gitlab-ctl hup unicorn
        bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-web sudo gitlab-ctl restart deploy-page
        bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-web sudo gitlab-ctl restart gitlab-pages
        bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-web sudo gitlab-ctl restart gitlab-workhorse
        bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-web sudo gitlab-ctl restart nginx
        bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-api sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure && sudo gitlab-ctl hup unicorn
        bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-api sudo gitlab-ctl restart gitlab-workhorse
        bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-api sudo gitlab-ctl restart nginx
        bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-api sudo gitlab-ctl restart registry
        bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-git sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure && sudo gitlab-ctl hup unicorn
        bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-git sudo gitlab-ctl restart gitlab-workhorse
        bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-git sudo gitlab-ctl restart nginx
        bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-registry sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure
        bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-registry sudo gitlab-ctl restart
      COMMAND
    end
  end

  context 'with custom node' do
    it 'outputs the right command' do
      stub_const('Steps::ServicesRestart::CUSTOM_RESTART_NODE', 'gstg-base-fe-web')

      expect { subject.run }.to output(command).to_stdout_from_any_process
    end

    def command
      command = <<~COMMAND
        bundle exec knife search 'roles:gstg-base-fe-registry' -a fqdn -Fjson
        bundle exec knife search 'roles:gstg-base-deploy-node' -a fqdn -Fjson
        bundle exec knife ssh -e -a fqdn 'fqdn:fqdn1' 'sudo cat /opt/gitlab/embedded/service/gitlab-rails/GITLAB_PAGES_VERSION 2>/dev/null'
        bundle exec knife ssh -e -a fqdn 'fqdn:fqdn2' 'sudo cat /opt/gitlab/embedded/service/gitlab-rails/GITLAB_PAGES_VERSION 2>/dev/null'
        bundle exec knife search 'roles:gstg-base-fe-registry' -a fqdn -Fjson
        bundle exec knife search 'roles:gstg-base-deploy-node' -a fqdn -Fjson
        bundle exec knife ssh -e -a fqdn 'fqdn:fqdn1' 'sudo cat /opt/gitlab/embedded/service/gitlab-rails/GITLAB_WORKHORSE_VERSION 2>/dev/null'
        bundle exec knife ssh -e -a fqdn 'fqdn:fqdn2' 'sudo cat /opt/gitlab/embedded/service/gitlab-rails/GITLAB_WORKHORSE_VERSION 2>/dev/null'
        bundle exec knife ssh -e -a fqdn roles:gstg-base-be-sidekiq sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure && sudo gitlab-ctl hup unicorn
        bundle exec knife ssh -e -a fqdn roles:gstg-base-be-sidekiq sudo gitlab-ctl restart sidekiq-cluster
        bundle exec knife ssh -e -a fqdn roles:gstg-base-be-sidekiq sudo gitlab-ctl restart nginx
        bundle exec knife ssh -e -a fqdn roles:gstg-base-be-mailroom sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure
        bundle exec knife ssh -e -a fqdn roles:gstg-base-be-mailroom sudo gitlab-ctl restart
        bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-web sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure && sudo gitlab-ctl hup unicorn
        bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-web sudo gitlab-ctl restart deploy-page
        bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-web sudo gitlab-ctl restart gitlab-pages
        bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-web sudo gitlab-ctl restart gitlab-workhorse
        bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-web sudo gitlab-ctl restart nginx
        bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-api sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure && sudo gitlab-ctl hup unicorn
        bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-api sudo gitlab-ctl restart gitlab-workhorse
        bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-api sudo gitlab-ctl restart nginx
        bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-api sudo gitlab-ctl restart registry
        bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-git sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure && sudo gitlab-ctl hup unicorn
        bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-git sudo gitlab-ctl restart gitlab-workhorse
        bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-git sudo gitlab-ctl restart nginx
        bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-registry sudo apt-get install -y -q --force-yes gitlab-ee=10.0.2-ee.0 && sudo gitlab-ctl reconfigure
        bundle exec knife ssh -e -a fqdn roles:gstg-base-fe-registry sudo gitlab-ctl restart
      COMMAND
    end
  end
end
