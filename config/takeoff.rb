# frozen_string_literal: true

require 'yaml'

module Takeoff
  extend self

  def configure
    yield(config)
  end

  def config
    @config ||= {}
  end

  def env
    @env ||= all_environments
  end

  def steps
    YAML.load_file("#{File.dirname(__FILE__)}/steps.yml")
  end

  def all_environments
    env = {}

    Dir.glob("#{File.dirname(__FILE__)}/environments/*.yml") do |env_file|
      env[File.basename(env_file, '.yml')] = YAML.load_file(env_file)
    end

    env
  end
end

Takeoff.configure do |config|
  config[:takeoff_repo] = 'git@gitlab.com:gitlab-org/takeoff.git'
  config[:dry_run] = ENV['DRY_RUN'] || false
  config[:verbose] = ENV['TAKEOFF_VERBOSE'] || false
  config[:use_package_server_key] = !%w[0 f false off].include?(ENV['USE_PACKAGE_SERVER_KEY'].to_s.downcase)
  config[:default_repo] = 'gitlab/pre-release'
  config[:default_stop_sidekiq] = false
  config[:slack_token] = ENV['TAKEOFF_SLACK_TOKEN']
  config[:slack_channel] = '#announcements'
  config[:slack_enabled] = true
  config[:knife_options] = '-e'
  config[:migration_threshold] = '60' # report migrations that take longer than this value, in seconds
  config[:log_level] = :debug
  config[:pidfile] = './takeoff.pid'
  config[:qa_notification_channels] = %w[#product #releases #development]
end
