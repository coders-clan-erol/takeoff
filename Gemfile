# frozen_string_literal: true

source 'https://rubygems.org'

gem 'chef', '~> 13.1'
gem 'chef-vault', '~> 3.0'
gem 'concurrent-ruby', '~> 1.0.5'
gem 'dotenv', '~> 2.2.1'
gem 'httparty', '~> 0.16.2'
gem 'net-ping',  '~>2.0.4'
gem 'rake', '~> 12.0'
gem 'semantic_logger', '~> 4.3.0'
gem 'tty-spinner', '~> 0.8'
gem 'tty-table', '~> 0.10.0'

group :development, :test do
  gem 'byebug', '~> 9.0'
  gem 'rubocop', '~> 0.56.0', require: false
  gem 'rubocop-rspec', '~> 1.25.1'
end

group :test do
  gem 'climate_control', '~> 0.2.0', require: false
  gem 'rspec', '~> 3.6'
  gem 'timecop', '~> 0.8'
end

group :development do
  gem 'guard-rspec', '~> 4.7', require: false
end

group :ed25519 do
  gem 'bcrypt_pbkdf', '~> 1.0'
  gem 'rbnacl', '~> 4.0'
  gem 'rbnacl-libsodium', '~> 1.0'
end
