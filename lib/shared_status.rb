require './lib/time_util'

class SharedStatus
  include TimeUtil

  TIME_LIMIT = 3600

  def initialize(start_time)
    @start_time = start_time
  end

  def user
    @user ||= username.strip
  end

  def total_time
    time_elapsed(time_now - @start_time, include_ago: false)
  end

  def long_time?
    (time_now - @start_time) > TIME_LIMIT
  end

  private

  def time_now
    Time.now.utc
  end

  def username
    ENV['GITLAB_USERNAME'] || `git config user.name`
  end
end
