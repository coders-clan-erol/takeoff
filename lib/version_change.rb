require 'json'

class VersionChange
  def initialize(role1, role2, version_file)
    @role1 = role1
    @role2 = role2
    @version_file = version_file
  end

  def changed?
    if Takeoff.config[:dry_run]
      puts knife_search_command(@role1)
      puts knife_search_command(@role2)
      puts knife_version_command('fqdn1')
      puts knife_version_command('fqdn2')

      return true
    end

    fqdn1 = hosts_for_role(@role1)
    fqdn2 = hosts_for_role(@role2)

    version(fqdn1) != version(fqdn2)
  end

  def old_version_ips
    return '1.2.3.4' if Takeoff.config[:dry_run]

    ips_for_role(@role1)
  end

  private

  def version(fqdn)
    `#{knife_version_command(fqdn)}`.strip.split.last
  end

  def hosts_for_role(role)
    row = JSON.parse(`#{knife_search_command(role)}`)['rows'].first

    # Get the first fqdn
    row.keys.first
  end

  def ips_for_role(role)
    JSON.parse(`#{knife_search_command(role)}`)['rows'].map do |row|
      row.values.first.values
    end.flatten.join (', ')
  end

  def knife_search_command(role)
    "bundle exec knife search 'roles:#{role}' -a fqdn -Fjson"
  end

  def knife_version_command(fqdn)
    "bundle exec knife ssh -e -a fqdn 'fqdn:#{fqdn}' 'sudo cat /opt/gitlab/embedded/service/gitlab-rails/#{@version_file} 2>/dev/null'"
  end
end
