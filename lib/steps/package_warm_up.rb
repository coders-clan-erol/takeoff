require './lib/steps/base'

module Steps
  class PackageWarmUp < Base
    def run
      CommandRunner.run_command_on_roles roles_to_warm,
                                         "sudo apt-get -qq update && sudo apt-get install -d -y -q --force-yes gitlab-ee=#{version}",
                                         title: "Downloading package on #{Roles.short_output(roles_to_warm)}",
                                         silence_stdout: true
    end

    private

    def roles_to_warm
      @roles_to_warm ||= roles.regular_and_blessed
    end

    def roles
      @roles ||= Roles.new(options[:environment])
    end

    def version
      @options[:version]
    end
  end
end
