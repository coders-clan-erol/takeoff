# frozen_string_literal: true

require './lib/steps/base_roles'
require './lib/steps/deploy_package'
require './lib/steps/unicorn_hup'
require './lib/steps/services_restart'

module Steps
  class DeployRestart < BaseRoles
    # TODO: Refactor this class

    def run
      deploy_and_restart
    end

    def role
      @role ||= options[:roles_to_deploy]
    end

    def resumer
      @options[:resumer]
    end

    private

    def deploy_and_restart
      yield_and_wait_until_reload(role, roles.services_for_role(role, skip: skip_services)) do
        steps = [Steps::DeployPackage.new(roles, roles_to_deploy: role, version: version),
                 Steps::ServicesRestart.new(roles, role: role, skip_services: skip_services)]

        steps.each do |step|
          step.spinners = options[:spinners]
          step.run!
        end

        errors = steps.map(&:error).compact

        @error = errors.join(', ') if errors.any?
      end
    end

    def skip_services
      @skip_services ||= begin
                           services_to_skip = []
                           services_to_skip << 'gitlab-pages' unless options[:pages_restart]
                           services_to_skip << 'gitlab-workhorse' unless options[:workhorse_restart]

                           services_to_skip
                         end
    end

    def yield_and_wait_until_reload(role, role_services)
      since = Time.now
      services_to_check = role_services.map { |service| "sudo gitlab-ctl status #{service}" }

      yield

      loop do
        sleep(5) unless Takeoff.config[:dry_run]
        cmd = "#{CommandRunner::KNIFE_COMMAND} 'roles:#{role}' " \
          "\"#{services_to_check.join(' && ')}\""

        cmd = "echo #{cmd}" if Takeoff.config[:dry_run]

        out = `#{cmd}`
        now = Time.now

        # Bail out once everything has restarted since the given time.
        all_restarted = out.scan(/(\d+)s; run:/).all? do |match|
          (now - match[0].to_i) >= since
        end

        break if all_restarted
      end
    end

    def version
      @options[:version]
    end
  end
end
