# frozen_string_literal: true

require 'cgi'
require 'httparty'

module Steps
  # Performs a QA Smoke Test against the current environment
  #
  # When the current environment has an entry in `ENVIRONMENT_MAP`, this step
  # will trigger a CI pipeline in the corresponding `gitlab-org/quality`
  # project.
  #
  # For example, if the current environment is `gstg`, a CI pipeline in
  # `gitlab-org/quality/staging` will be triggered using the
  # `SMOKE_TEST_STAGING_TRIGGER` environment variable.
  #
  # If the current environment has no entry in `ENVIRONMENT_MAP`, this step is a
  # no-op.
  class SmokeTest < BaseRoles
    ENDPOINT = 'https://gitlab.com/api/v4/projects'

    ENVIRONMENT_MAP = {
      gstg: 'staging'
    }

    def run
      return unless testable_environment?

      if trigger_token.empty?
        logger.warn("#{trigger_token_name} needs to be set to trigger a smoke test!", environment: quality_environment)

        return
      end

      result = HTTParty.post(trigger_url, body: {
        token: trigger_token,
        ref: 'master'
      })

      if result.success?
        logger.info("QA smoke test initiated", environment: quality_environment, url: result['web_url'])
      else
        logger.warn("QA smoke test failed", environment: quality_environment, message: result['message'])
      end
    end

    private

    def environment
      @environment ||= options[:environment].to_sym
    end

    def testable_environment?
      ENVIRONMENT_MAP.key?(environment)
    end

    def quality_environment
      ENVIRONMENT_MAP.fetch(environment)
    end

    def quality_project
      "gitlab-org/quality/#{quality_environment}"
    end

    def trigger_url
      @trigger_url ||=
        [ENDPOINT, CGI.escape(quality_project), 'trigger', 'pipeline'].join('/')
    end

    def trigger_token_name
      @trigger_token_name ||= "smoke_test_#{quality_environment}_trigger".upcase
    end

    def trigger_token
      @trigger_token ||= ENV.fetch(trigger_token_name, '')
    end
  end
end
