# frozen_string_literal: true

require './lib/steps/base_roles'

module Steps
  class DeployGitaly < BaseRoles
    def run
      return unless roles_to_deploy

      unless version_changed?
        puts ANSI::BLUE % "Gitaly version has not changed. Host(s): #{version_change.old_version_ips} will remain on the old version."

        return
      end

      gitaly_step = Steps::DeployRestart.new(roles, roles_to_deploy: roles.gitaly_roles, version: version)

      gitaly_step.run!

      @error = gitaly_step.error
    end

    private

    def version_change
      @version_change ||= VersionChange.new(roles.gitaly_roles, roles.blessed_node, 'GITALY_SERVER_VERSION')
    end

    def version_changed?
      @version_changed ||= version_change.changed?
    end

    def roles_to_deploy
      @roles_to_deploy ||= roles.gitaly_roles
    end

    def version
      @options[:version]
    end
  end
end
