# frozen_string_literal: true

require './lib/steps/base_roles'

module Steps
  class DeployPackage < BaseRoles
    def run
      run_command_on_roles roles_to_deploy,
                           deploy_command,
                           title: title,
                           spinners: spinners
    end

    private

    def deploy_command
      cmd = "sudo apt-get install -y -q --force-yes gitlab-ee=#{version} && sudo gitlab-ctl reconfigure"
      cmd += ' && sudo gitlab-ctl hup unicorn' if suitable_roles_to_hup?

      cmd
    end

    def suitable_roles_to_hup?
      roles.service_roles['unicorn'].include?(roles_to_deploy) || roles_to_deploy.is_a?(Array)
    end

    def roles_to_deploy
      options[:roles_to_deploy] ||= roles.blessed_node
    end

    def title
      options[:roles_to_deploy] ? "Deploying gitlab-ee #{version} on #{roles_to_deploy}" : "Deploying gitlab-ee #{version} on the blessed node"
    end

    def version
      @options[:version]
    end
  end
end
